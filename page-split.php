<?php
/**
 * Template Name: Split Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdogcustomizer
 */

get_header(); ?>

			
	
	<div class="big-background">

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				
			</header><!-- .entry-header -->

			<div class="entry-content">
				<h1><?php the_title(); ?></h1>
				<?php if(get_field('post_author_name')) {?>
  <small>written by: <?php the_field('post_author_name'); ?></small>
  <hr/>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('post_author_name')) {?>
    
<?php }?> <!-- ends the second outer condition -->  
				<?php the_content(); ?>
				<div class="left-side">
				<?php the_field('left_section'); ?>	
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdogcustomizer' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- ends left side -->
			<div class="right-side">
				<?php the_field('right_section'); ?>
				
			</div><!-- ends right side-->
			</div><!-- .entry-content -->
	

	<?php edit_post_link( __( 'Edit', 'plasterdogcustomizer' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>
