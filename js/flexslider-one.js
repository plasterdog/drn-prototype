jQuery(window).load(function() {
  jQuery('.flexslider').flexslider({
    animation: "fade",
    directionNav: true,
    slideshowSpeed: 5000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
	animationSpeed: 400,
	animationLoop: true,
	pauseOnHover:true,
	slideshow: true,
	prevText: " ", 
	nextText: " ", 
	smoothHeight:false
  });
});



