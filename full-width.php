<?php
/*
*Template Name: Full Width
 * @package plasterdogcustomizer
 */

get_header(); ?>
	

		<div class="big-background">

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->

<?php get_footer(); ?>
