<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package plasterdogcustomizer
 */
?>

	</div><!-- #content -->
</div><!-- #page -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="foot-constraint">
					<div class="footer-menu clear">
						<!-- https://codex.wordpress.org/Function_Reference/has_nav_menu -->
						 <?php
						if ( has_nav_menu( 'footer' ) ) {
						     wp_nav_menu( array( 'theme_location' => 'footer' ) );
						} ?> 
					</div>	

			<div class="left-side">
							<?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?>
							<?php endif; // end sidebar widget area ?>
			</div>

			<div class="right-side">
							<?php if ( ! dynamic_sidebar( 'sidebar-5' ) ) : ?>
							<?php endif; // end sidebar widget area ?>
			</div>
</div><!-- ends foot constraint -->
			<div class="site-info">
<div id="second-foot-constraint">
			<div class="left-side">
			<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone') ?></span><br/>  &copy; <?php $the_year = date("Y"); echo $the_year; ?> | All rights reserved<?php } ?>	
			<?php if(!get_option('pdog_phone')) {?>	&copy; <?php $the_year = date("Y"); echo $the_year; ?>	| All rights reserved	<?php }?>
			</div>

			<div class="right-side" style="text-align:right;">
			<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_address') ?></span><br/><?php bloginfo( 'name' ); ?>  | <?php bloginfo( 'description' ); ?> <?php } ?>
			<?php if(!get_option('pdog_address')) {?>	<?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?>		<?php }?>
			</div>	
</div><!-- ends second foot constraint -->
			</div><!-- .site-info -->
		

		
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
