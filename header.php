<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package plasterdogcustomizer
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<?php if(get_option('pdog_analytics') && get_option('pdog_analytics') != '') {?>
		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo get_option('pdog_analytics') ?>"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '<?php echo get_option('pdog_analytics') ?>');
		</script>
<?php } ?>
	
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="shortcut icon" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/favicon.ico" type="image/x-icon"/>
<!--[if lt IE 9]>
<script>
  var e = ("abbr,article,aside,audio,canvas,datalist,details," +
    "figure,footer,header,hgroup,mark,menu,meter,nav,output," +
    "progress,section,time,video,main").split(',');
  for (var i = 0; i < e.length; i++) {
    document.createElement(e[i]);
  }
</script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
<?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>	
<div id="full-top" style="background-image:url(<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>);  background-repeat:no-repeat;  background-position:center top; background-size:cover;">
<?php else : ?>
<div id="full-top">
<?php endif; ?>	
	<div id="upper-band">



<div class="masthead-holder">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">


<!-- inserts the header image-->
	<div id="left-head-1">

						<?php $header_image = get_header_image();
						if ( ! empty( $header_image ) ) { ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" />
						</a>
						<?php } // if ( ! empty( $header_image ) ) ?>

	</div><!-- ends left head 1 -->
<div id="left-head">

	<div id="left-head-2">


</div><!-- ends left head 2-->
</div><!-- end left head-->
<div id="right-head">
					<?php if(get_option('pdog_headerlink') && get_option('pdog_headerlink') != '') {?>
					<a href="<?php echo get_option('pdog_headerlink') ?>"><h3>LOGIN</h3></a><?php } ?>


			<div class="social">
				<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone') ?></span>  <?php } ?>	
			<?php if(!get_option('pdog_phone')) {?>	&nbsp;		<?php }?>
				<ul class="top-social-icons">

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<li><a href="<?php echo get_option('pdog_facebook') ?>" target="_blank" ><i class="fab fa-facebook-square"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fab fa-linkedin"></i></a>	</li><?php } ?>	
						
					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fab fa-twitter-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fab fa-instagram"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fab fa-youtube"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fab fa-vimeo-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>" target="_blank"><i class="fas fa-envelope"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_meetup') && get_option('pdog_meetup') != '') {?>
					<li><a href="<?php echo get_option('pdog_meetup') ?>" target="_blank"><i class="fab fa-meetup"></i></a>	</li><?php } ?>						
				</ul>
			</div><!-- ends social -->
		</div><!--ends right head -->			





				</div><!-- ends site branding -->
	</header><!-- ends masthead -->
</div><!-- ends masthead holder -->


</div><!-- ends upper band-->
	</header><!-- #masthead -->

</div> <!-- ends full top -->


