<?php
/**
 * Template Name: Archive Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdogcustomizer
 */


get_header(); ?>

<div class="big-background">
		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<h1><?php the_title(); ?></h1>	
	
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdogcustomizer' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdogcustomizer' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
			<?php endwhile; // end of the loop. ?>
			<hr/>
			<?php if ( get_field( 'category_slug_name' ) ): ?>
	
					<?php
					global $post;
					$args = array( 'numberposts' =>get_field('number_of_excerpts'), 'offset'=> 0, 'category_name' =>get_field('category_slug_name'), 'orderby' => 'post_date', 'order' => 'DSC');
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) :	setup_postdata($post); ?>
							
			<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
			

				<?php if (!empty($post->post_excerpt)) : ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
			<div class="archive_left_picture">	
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
			</div><!-- ends left picture -->
				<div class="archive_right_text">
				<header class="entry-header">
				<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
				</header><!-- .entry-header -->	
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
			<div class="archive_left_picture">	
			<?php the_post_thumbnail( 'medium' ); ?>
			</div><!-- ends left picture -->
				<div class="archive_right_text">
				<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>	
				</header><!-- .entry-header -->
				<?php the_content(); ?>
				<?php endif; ?>	
				</div><!-- ends right text -->
			</article><!-- #post-## -->
		    <div class="clear"><hr/></div>

			<?php   } else { ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  


				<?php if (!empty($post->post_excerpt)) : ?>
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->		
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
		<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>	
		</header><!-- .entry-header -->			
				<?php the_content(); ?>
				<?php endif; ?>	
			</article><!-- #post-## -->	
			<div class="clear"><hr/></div>
		 	<?php    } ?>

			<?php endforeach; ?>

<?php else: // field_name returned false ?>	


<!--if no entry in field query will show posts unfiltered by category -->	

			<?php if ( get_field( 'number_of_excerpts' ) ): ?>
			<?php 
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page' =>get_field('number_of_excerpts'))); ?>

			<?php else: // field_name returned false ?>	
			<?php 
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
			<?php endif; // end of if field_name logic ?>	
			
				<!-- the loop -->
			<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
					
				<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
				<div class="archive_left_picture">	
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
				</div><!-- ends left picture -->
					<div class="archive_right_text">
					<header class="entry-header">
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
					</header><!-- .entry-header -->
				<?php if (!empty($post->post_excerpt)) : ?>
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
				<?php the_content(); ?>
				<?php endif; ?>	
					</div><!-- ends right text -->
				</article><!-- #post-## -->
			    <div class="clear"><hr/></div>

				<?php   } else { ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
					<header class="entry-header">
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
					</header><!-- .entry-header -->
				<?php if (!empty($post->post_excerpt)) : ?>
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
				<?php the_content(); ?>
				<?php endif; ?>	
				</article><!-- #post-## -->
				<div class="clear"><hr/></div>		
			 	<?php    } ?>

				<?php endwhile; ?>
				<!-- end of the loop -->

<?php endif; // end of if field_name logic ?>	
<?php wp_reset_postdata(); ?>


		</main><!-- #main -->
	</div><!-- #primary -->

	<div id="secondary" class="widget-area front-book-array" role="complementary">

			<?php if ( ! dynamic_sidebar( 'sidebar-3' ) ) : ?>
			<?php endif; // end sidebar widget area ?>

	</div><!-- #secondary -->
	<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>
