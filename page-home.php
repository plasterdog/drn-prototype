<?php
/**
 * Template Name: Landing Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdogcustomizer
 */

get_header(); ?>

<!-- THREE FEATURED ITEMS BELOW CONTENT REGION -->

<div class="big-background">
<!-- making the region conditional based on entry -->
<?php if ( $post->post_content!=="" ) {		?>		

		<!--THE CONTENT-->
		<div id="page" class="hfeed site" >
			<div id="content" class="site-content" >
			<div id="primary" class="full-content-area" >
				<main id="main" class="full-site-main" role="main">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>	
				</main><!-- #main -->
			</div><!-- #primary -->
			</div><!-- #content -->
			</div><!-- #page -->
			<div class="clear"></div>
			<?php } ?><!-- ending the condition -->
		
		<!--- THE HERO IMAGE SECTION -->
		<div class="hero-image clear">
			<img src="<?php the_field('landing_page_hero_image'); ?>"/>
			<div class="hero-caption-link"><h4><a href="<?php the_field('hero_call_to_action_target'); ?>"><?php the_field('hero_call_to_action_label'); ?></a>
			</h4>
			</div><!-- ends hero caption -->
		</div>	<!-- ends hero image -->

		
		<div class="clear front-section"><!--THE PRIMARY THREE ITEMS-->
				<ul class="three-focus-items">
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('first_background_color'); ?>">	
						<div class="focus-item-content">		
						<a href="<?php the_field('first_feature_link'); ?>"><h3><?php the_field('first_feature_title'); ?> </h3></a>
						<div class="focus-item-excerpt"><?php the_field('first_feature_excerpt'); ?></div><!--ends focus item excerpt -->
						<h4 style="background-color:<?php the_field('first_background_color'); ?>"><a href="<?php the_field('first_feature_link'); ?>"><?php the_field('first_feature_link_label'); ?></a></h4>	
						</div><!-- ends focus item content -->
					</div>
					</li>
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('second_background_color'); ?>">	
						<div class="focus-item-content">	
						<a href="<?php the_field('second_feature_link'); ?>"><h3><?php the_field('second_feature_title'); ?> </h3></a>
						<div class="focus-item-excerpt"><?php the_field('second_feature_excerpt'); ?></div><!--ends focus item excerpt -->
						<h4 style="background-color:<?php the_field('second_background_color'); ?>"><a href="<?php the_field('second_feature_link'); ?>"><?php the_field('second_feature_link_label'); ?></a></h4>	
						</div><!-- ends focus item content -->
					</div>
					</li>
					<li>		
					<div class="inner-focus-item" style="background-color:<?php the_field('third_background_color'); ?>">
						<div class="focus-item-content">
						<a href="<?php the_field('third_feature_link'); ?>"><h3><?php the_field('third_feature_title'); ?> </h3></a>
						<div class="focus-item-excerpt"><?php the_field('third_feature_excerpt'); ?></div><!--ends focus item excerpt -->
						<h4 style="background-color:<?php the_field('third_background_color'); ?>"><a href="<?php the_field('third_feature_link'); ?>"><?php the_field('third_feature_link_label'); ?></a></h4>
						</div><!-- ends focus item content -->
					</div>	
					</li>			
				</ul>
		</div><!-- ends section -->

		<div class="clear">
				<?php if(get_field('slider_intro_statement')) {?>	
				<h3 class="section-intro"><?php the_field('slider_intro_statement'); ?></h3>
				<?php }	?>
		</div><!-- ends section -->

<!--- THE SLIDER -->

<div class="slider-container clear front-section">
		<div id="slider" class="flexslider">         
                <ul class="slides">
                    <?php
                    // check if the repeater field has rows of data
                    if( have_rows('top_slider_repeater') ): ?>                  

                    <?php while ( have_rows('top_slider_repeater') ) : the_row(); ?>

                    <li>
                    <h2>	<?php echo the_sub_field('top_title'); ?></h2>
                    <div class="left-side-section">	
                       <img src="<?php the_sub_field('left_side_image'); ?>"/>
                	</div><!-- ends left side section -->

                    <div class="right-side-section">  
                    	<div class="inner-right-section">         
                    	<h3><?php echo the_sub_field('right_side_title'); ?></h3>
                    	<?php echo the_sub_field('right_side_excerpt'); ?>
                    	<h3><a href="<?php the_sub_field('right_side_link_target'); ?> "><?php echo the_sub_field('right_side_link_label'); ?></a></h3>
                    	</div><!--ends inner right section-->       	
                    </div><!-- ends right side section  -->
                    
                    </li>
                                                   	                
	                 <?php endwhile; ?>
	                 <?php else : ?>

	                <?php  // no rows found
	                 endif; ?> 
	                 </ul>
	        </div><!--- ends flexslider -->
	    </div>


		<div class="clear">
				<?php if(get_field('partnership_intro_statement')) {?>	
				<h3 class="section-intro"><?php the_field('partnership_intro_statement'); ?></h3>
				<?php }	?>
		</div><!-- ends section -->

			<div class="clear front-section partners"><!--PARTNER LOGO SECTION -->
			        <?php
                    // check if the repeater field has rows of data
                    if( have_rows('associated_logo_links') ): ?>                  

                    <?php while ( have_rows('associated_logo_links') ) : the_row(); ?>
                    <li>
                    <a href="<?php echo the_sub_field('associated_logo_link_target'); ?>">
                    <img src="<?php echo the_sub_field('associated_logo_image'); ?>"></a>
                    </li>
	                <?php endwhile; ?>
	                <?php else : ?>

	                <?php  // no rows found
	                 endif; ?> 
	        </div><!-- ends section -->

		<div class="clear">
				<?php if(get_field('secondary_feature_intro')) {?>	
				<h3 class="section-intro"><?php the_field('secondary_feature_intro'); ?></h3>
				<?php }	?>
		</div><!-- ends section -->


			<div class="clear front-section"><!--SECONDARY FEATURE SECTION -->	        
				<ul class="four-focus-items">
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('first_secondary_background_color'); ?>">		
						<div class="focus-item-content">	
						<a href="<?php the_field('first_secondary_link_target'); ?>"><h3><?php the_field('first_secondary_title'); ?> </h3></a>
						<div class="focus-item-excerpt"><?php the_field('first_secondary_excerpt'); ?></div><!--ends focus item excerpt -->
						<h4 style="background-color:<?php the_field('first_secondary_background_color'); ?>"><a href="<?php the_field('first_secondary_link_target'); ?>"><?php the_field('first_secondary_link_label'); ?></a></h4>	
						</div><!-- ends focus item content -->
					</div>
					</li>
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('second_secondary_background_color'); ?>">	
						<div class="focus-item-content">		
						<a href="<?php the_field('second_secondary_link_target'); ?>"><h3><?php the_field('second_secondary_title'); ?> </h3></a>
						<div class="focus-item-excerpt"><?php the_field('second_secondary_excerpt'); ?></div><!--ends focus item excerpt -->
						<h4 style="background-color:<?php the_field('second_secondary_background_color'); ?>"><a href="<?php the_field('second_secondary_link_target'); ?>"><?php the_field('second_secondary_link_label'); ?></a></h4>	
						</div><!-- ends focus item content -->
					</div>
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('third_secondary_background_color'); ?>">	
						<div class="focus-item-content">		
						<a href="<?php the_field('third_secondary_link_target'); ?>"><h3><?php the_field('third_secondary_title'); ?> </h3></a>
						<div class="focus-item-excerpt"><?php the_field('third_secondary_excerpt'); ?></div><!--ends focus item excerpt -->
						<h4 style="background-color:<?php the_field('third_secondary_background_color'); ?>"><a href="<?php the_field('third_secondary_link_target'); ?>"><?php the_field('third_secondary_link_label'); ?></a></h4>	
						</div><!-- ends focus item content -->
					</div>
					</li>
					<li>
					<div class="inner-focus-item" style="background-color:<?php the_field('fourth_secondary_background_color'); ?>">	
						<div class="focus-item-content">		
						<a href="<?php the_field('fourth_secondary_link_target'); ?>"><h3><?php the_field('fourth_secondary_title'); ?> </h3></a>
						<div class="focus-item-excerpt"><?php the_field('fourth_secondary_excerpt'); ?></div><!--ends focus item excerpt -->
						<h4 style="background-color:<?php the_field('fourth_secondary_background_color'); ?>"><a href="<?php the_field('fourth_secondary_link_target'); ?>"><?php the_field('fourth_secondary_link_label'); ?></a></h4>	
						</div><!-- ends focus item content -->
					</div>
					</li>		
				</ul>
	        </div><!-- ends section -->

		<div class="clear">
				<?php if(get_field('signup_form_intro')) {?>	
				<h3 class="section-intro"><?php the_field('signup_form_intro'); ?></h3>
				<?php }	?>
		</div><!-- ends section -->

	        <div class="clear front-section signup-section"><!--SIGNUP SECTION -->
	        	<?php
				if ( get_field('signup_form_shortcode') ) {
				echo do_shortcode( get_field('signup_form_shortcode') );
				}
				?>
	        </div><!-- ends section -->

<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->

<?php get_footer(); ?>
