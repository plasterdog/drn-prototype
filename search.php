<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package plasterdogcustomizer
 */

get_header(); ?>

		<div class="clear"></div>
<div class="big-background" style="min-height:800px;background-attachment: fixed; background-position:top center; background-repeat: no-repeat;background-size:cover;background-image: url(<?php the_field('home_background_image'); ?>);">
		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'plasterdogcustomizer' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>

			<?php plasterdogcustomizer_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>
<div class="clear"><hr/></div>
		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
	<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>
